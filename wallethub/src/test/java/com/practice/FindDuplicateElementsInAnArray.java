package com.practice;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class FindDuplicateElementsInAnArray {

	public static void main(String[] args) {
			int s[] = {1,5,5,7,8,9,6,5,4,3,4};
//			int count = 0;
//			for (int i=0; i<s.length;i++) {
//				for(int j=0;j<s.length;j++) {
//					if(s[i]==(s[j])) {
//						count++;
//					}
//					
//				}
//				//System.out.println(s[i] + " is repeated " + count + " times");
//				count = 0;
//			}
			
			
			//storing in map
			Map<Integer,Integer> m = new HashMap<Integer,Integer>();
			for (int i=0; i<s.length;i++) {
					if(m.containsKey(s[i])) {
						m.put(s[i], m.get(s[i])+1);
					}
					else {
						m.put(s[i], 1);
					}
				}
			Set<Integer> a = m.keySet();
//			Iterator<Integer> t = a.iterator();
//			while(t.hasNext()) {
//				System.out.println(t.next() + " is repeated " + m.get(t.next()) + " times");
//			}
			for(Integer t1 : a) {
			System.out.println(t1  + " is repeated " + m.get(t1) + " times");
	}
	}

}
