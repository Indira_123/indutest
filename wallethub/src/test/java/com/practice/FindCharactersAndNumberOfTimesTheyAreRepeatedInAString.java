package com.practice;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class FindCharactersAndNumberOfTimesTheyAreRepeatedInAString {

	public static void main(String[] args) {
		String s = "TesTing aaaa bbccvn tttttttt";
		int count = 0;
//		for(int i=0;i<s.length();i++) {
//			for(int j= 0;j<s.length();j++) {
//				if(s.charAt(i)==s.charAt(j)) {
//					count++;
//				}
//				
//			}
//			System.out.println(s.charAt(i) + " is repeated " + count + " times");
//			count=0;
//		}
		//storing in map
		
		Map<Character,Integer> m = new HashMap<Character,Integer>();
		for (int i=0; i<s.length();i++) {
				if(m.containsKey(s.charAt(i))) {
					m.put(s.charAt(i), m.get(s.charAt(i))+1);
				}
				else {
					m.put(s.charAt(i), 1);
				}
			}
		Set<Character> a = m.keySet();
//		Iterator<Integer> t = a.iterator();
//		while(t.hasNext()) {
//			System.out.println(t.next() + " is repeated " + m.get(t.next()) + " times");
//		}
		for(Character t1 : a) {
			if(m.get(t1)>1) {
				System.out.println(t1  + " is repeated " + m.get(t1) + " times");
			}
}
}

}
