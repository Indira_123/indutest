package com.practice;

public class ReverseStringI {

	public String reverseStringOne(String s) {
		String b = "";
		String ar[] = s.split(" ");
		for (int a = 0; a < ar.length; a++) {
			int l = ar[a].length();

			for (int i = l; i > 0; i--) {
				b = b + (ar[a].charAt(i - 1));
			}
			b = b + " ";
		}
		return b;

	}
	
	public String reverseStringTwo(String s) {
		StringBuffer b = new StringBuffer(s);
		String c = b.reverse().toString();
		return c;
	}

	public static void main(String[] args) {

		ReverseStringI rs = new ReverseStringI();
		System.out.println(rs.reverseStringOne("abc inh"));
		System.out.println(rs.reverseStringTwo("abc inh"));
	}

}
