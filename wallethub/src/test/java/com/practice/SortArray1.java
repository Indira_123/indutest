package com.practice;

import java.util.Arrays;

public class SortArray1 {

	public static void main(String[] args) {
			int[] a = {4,3,7,6,5,2};
			
			Arrays.sort(a);
			for(int i=0;i<a.length;i++) {
			System.out.print(a[i] + " ");
			}
			
			//Another way
			int[] b = {4,3,7,6,5,2};
			for(int i=0;i<b.length;i++) {
				for(int j=i+1;j<b.length;j++) {
					if(b[i]>b[j]) {
						b[i]=b[i]+b[j];
						b[j]=b[i]-b[j];
						b[i]=b[i]-b[j];
					}
					
				}
				System.out.print(b[i] + " ");
			}
			
	}

}
