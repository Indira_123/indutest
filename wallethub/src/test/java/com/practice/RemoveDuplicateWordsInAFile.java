package com.practice;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class RemoveDuplicateWordsInAFile {

	public static void main(String[] args) throws IOException {
		HashSet<String> uniqueNames = new HashSet<String>();
		ArrayList<String> outputLines = new ArrayList<String>();

		Scanner input = new Scanner(new File("C:\\Users\\Indira Swarangi\\OneDrive\\Documents\\input.txt"));
		while (input.hasNextLine()) {

			// Split a line into an array of names.
			String[] names = input.nextLine().split(" ");
			String edited = "";
			for (int i = 0; i < names.length; i++) {

				// If the name is already in the set, remove it.
				if (uniqueNames.add(names[i])) {
					edited += names[i] + " ";
				}
			}

			edited = edited.trim(); // remove excess whitespace

			// Add the final line to our output array.
			if (!edited.equals("")) {
				outputLines.add(edited);
			}
		}

		// Write the output array to a file.
		String outputFn = "output.txt";
		FileWriter fileWriter = new FileWriter(outputFn);
		BufferedWriter output = new BufferedWriter(fileWriter);
		output.write(String.join("\n", outputLines));
		output.close();
		System.out.println("File '" + outputFn + "' created!");

	}

}
