package com.practice.two;

import java.util.Random;

public class RandomNumGenerate {

	public static void main(String[] args) {

		Random random = new Random();
		System.out.println(random.nextDouble());
		System.out.println(random.nextBoolean());
		System.out.println(random.nextFloat());
		System.out.println(random.nextGaussian());
		System.out.println(random.nextInt());
		System.out.println(random.nextInt(6));
		System.out.println(random.nextLong());
	}

}
