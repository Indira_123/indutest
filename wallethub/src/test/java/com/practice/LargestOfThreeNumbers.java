package com.practice;

public class LargestOfThreeNumbers {

	public static void main(String[] args) {
			int a = 33;
			int b = 13;
			int c = 43;
			
			if(a>b && a>c) {
				System.out.println("a is greater");
			}
			
			else if(b>a && b>c) {
				System.out.println("b is greater");
			}
			
			else {
				System.out.println("c is greater");
			}
			
			//Another way
			int largest = a>b?a:b;
			largest = c>largest?c:largest;
			System.out.println("largest number is "+largest);
			
	}

}
