package com.practice;

import java.util.Scanner;

public class PalindromeString {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter string");
		String s = sc.next();
		String a = s;
		String r="";
		for(int i=s.length()-1;i>=0;i--) {
			r = r + s.charAt(i);
		}
		if(r.equalsIgnoreCase(a)) {
			System.out.println("Palindrome String");
		}
		else {
			System.out.println("Not Palindrome String");
		}
	}
}
