package com.practice;

public class NumberOfDigitsInANumber {

	public static void main(String[] args) {
		int a = -8765;
		int count=0;;
		
		while(a!=0) {
			a=a/10;
			count++;
		}
		System.out.println(count);
	}

}
