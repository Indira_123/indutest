package com.practice;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class FindDuplicateWordsInAFile {

	public static void main(String[] args) throws IOException {
		File file = new File("C:\\Users\\Indira Swarangi\\OneDrive\\Documents\\input.txt");
		FileReader r = new FileReader(file);
		BufferedReader read = new BufferedReader(r);
		String s;
		String[] b;
		Map<String, Integer> a = new HashMap<String, Integer>();
		while (read.ready()) {
			s = read.readLine();
			b = s.split(" ");
			for (String t : b) {
				if (a.containsKey(t)) {
					a.put(t, a.get(t)+1);
				} else {
					a.put(t, 1);
				}
			}

		}

		Set<String> h = a.keySet();
		for (String g : h) {
			System.out.println(g + " is repeated " + a.get(g) + " times");
		}
		read.close();
	}

}
