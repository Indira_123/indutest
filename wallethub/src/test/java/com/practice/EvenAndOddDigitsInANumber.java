package com.practice;

public class EvenAndOddDigitsInANumber {

	public static void main(String[] args) {
		int a = 123456676;
		int n=0;
		int even=0;
		int odd=0;
		while(a!=0) {
			n=a%10;
			
			if(n%2==0) {
				even++;
			}
			else {
				odd++;
			}
			a=a/10;
		}
		
		System.out.println("Number of evens are : "+even);
		System.out.println("Number of odds are : "+odd);
	}

}
