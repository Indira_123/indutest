package com.practice;

public class ReverseANumber {

	public static void main(String[] args) {
		
		int a = 98765456;
		int r=0;
		while(a!=0) {
			r=r*10+a%10;
			a=a/10;
		}
		System.out.println(r);
		
		//using stringbuffer
		a = 98765456;
		String b = String.valueOf(a); 
		StringBuffer s = new StringBuffer(b);
		String c= s.reverse().toString();
		System.out.println(c);
	}
	
	
	

}
