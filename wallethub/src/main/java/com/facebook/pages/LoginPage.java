package com.facebook.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage 
{
    WebDriver driver;
    
    public LoginPage(WebDriver driver) {
    	this.driver = driver;
    	PageFactory.initElements(driver, this);
    }
    
    //username webelement
    @FindBy (xpath="//input[@id='email']")
    public WebElement user;
    
    //password webelement
    @FindBy (xpath="//input[@id='pass']")
    public WebElement pwd;
    
    //logIn button webelement
    @FindBy (xpath="//button[@name='login']")
    public WebElement logInButton;
    
    
    
    
    public void loginFacebook(String userName, String password) {
    	
    	user.clear();
    	user.sendKeys(userName);
    	
    	pwd.clear();
    	pwd.sendKeys(password);
    	
    	logInButton.click();    	
    }
	
}
