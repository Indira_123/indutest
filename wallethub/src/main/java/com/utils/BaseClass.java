package com.utils;

import java.util.HashMap;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class BaseClass {

	WebDriver driver;

	public WebDriver launchBrowser(String url, String browserName) {

		if (browserName.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", ".//Drivers//chromedriver.exe");

			ChromeOptions options = new ChromeOptions();
			options.setAcceptInsecureCerts(true);
			options.addArguments("--disable-notifications");
			//options.addArguments("--disable-web-security");
			options.setExperimentalOption("excludeSwitches", new String[] { "enable-automation" });

			HashMap<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("credentials_enable_service", false);

			options.setExperimentalOption("prefs", prefs);
			driver = new ChromeDriver(options);

		}
		if (browserName.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", ".//Drivers//chromedriver.exe");
			driver = new FirefoxDriver();

		}
		if (browserName.equalsIgnoreCase("ie")) {
			System.setProperty("webdriver.ie.driver", ".//Drivers//chromedriver.exe");
			driver = new InternetExplorerDriver();

		}
		driver.manage().window().maximize();
		driver.get(url);
		return driver;
	}

}
