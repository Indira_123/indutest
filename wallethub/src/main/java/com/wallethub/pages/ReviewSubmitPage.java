package com.wallethub.pages;

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ReviewSubmitPage {
	WebDriver driver;
	
	public ReviewSubmitPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy (xpath = "//div[contains(@class,'dropdown second')]")
	WebElement dropdown;
	
	@FindBy (xpath = "//div[contains(@class,'dropdown second')]/ul/li")
	List<WebElement> insuranceTypeList;
	
	@FindBy (xpath = "//div[@class='android']/textarea")
	WebElement reviewSubmitTextarea;
	
	@FindBy (xpath = "//div[contains(text(),'Submit')]")
	WebElement submitButton;
	
	
	public void selectInsuranceType(String insuranceName) {
		reviewSubmitTextarea.click();
		
		String insuranceType=null;
		dropdown.click();
		Iterator<WebElement> itlIter = insuranceTypeList.iterator();
		while (itlIter.hasNext()) {
			WebElement insurance = itlIter.next();
			insuranceType = insurance.getText();
			if (insuranceType.equalsIgnoreCase(insuranceName)){
				insurance.click();
			}
			
		}
	}
	
	
	public void submitYourReview(String review) {
		reviewSubmitTextarea.click();
		reviewSubmitTextarea.clear();
		reviewSubmitTextarea.sendKeys(review);
		submitButton.click();
	}

	
}
