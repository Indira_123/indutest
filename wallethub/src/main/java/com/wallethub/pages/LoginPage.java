package com.wallethub.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
	
	WebDriver driver;
	
	public LoginPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy (xpath = "//a[contains(text(),'Login')]")
	WebElement login;
	
	
	@FindBy (xpath = "//input[@aria-label='email address']")
	WebElement email;

	@FindBy (xpath = "//input[@type='password']")
	WebElement pwd;
	
	@FindBy (xpath = "//button/child::span/span[contains(text(),'Login')]")
	WebElement loginButton;
	
	
	public void loginToWalletHub(String emainID, String password) {
		login.click();
		email.clear();
		email.sendKeys(emainID);
		pwd.clear();
		pwd.sendKeys(password);
		loginButton.submit();
		
		
	}
	

}
