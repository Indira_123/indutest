package com.wallethub.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TestInsuranceCompanyReviewPage {

	WebDriver driver;
	public TestInsuranceCompanyReviewPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	
	
	
	@FindBy (xpath="//*[@class='rvs-svg']//*[local-name()='svg']")
	List<WebElement> stars;
	
	@FindBy (xpath = "//*[@class='rvs-svg']//*[local-name()='path'][2]")
	List<WebElement> starColor;
	
	@FindBy (xpath = "//span[contains(text(),'Your Review')]/parent::div/parent::div/following-sibling::div[contains(@class,'text')]")
	WebElement reviewText;
	
	
	public WebDriver goToTestInsuranceCompanyReviewURL(WebDriver driver, String url) {
		driver.get(url);
		return driver;
	}
	
	
	public void selectRating(int rating) {
		stars.get(rating-1).click();
	}
	
	
	
	public boolean hoverAndVerifyStarsLitUp(String color) {
		Actions action = new Actions(driver);
		int count=0;
		for (int j=0; j<5; j++) {
			action.moveToElement(stars.get(j)).build().perform();
			String colorCode = starColor.get(j).getAttribute("stroke");
			if (colorCode.equalsIgnoreCase(color)) {
				count++;
			}
		}
		if (count==5)
			return true;
		else
			return false;
	}
	
	
	public String getReviewText() {
		return reviewText.getText();
	}
}
