package com.wallethub.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProfilePage {
	WebDriver driver;
	
	public ProfilePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy (xpath = "//*[@class='rvs-star-svg']//*[local-name()='path'][2]")
	List<WebElement> givenRating;
	
	@FindBy (xpath = "//a[contains(text(),'Test Insurance Company')]")
	WebElement navigateToReviewButton;
	
	@FindBy (xpath = "//span[contains(text(),'Your Review')]")
	WebElement yourReviewText;
		
	
	public boolean verifyGivenRating(int giveRating) {
		int rating = givenRating.size();
		if (rating == giveRating) {
			return true;
		}else
			return false;
	}
	
	public boolean verifyIfNavigatedToProfilePage(String profileUrl) {
		String actualUrl = driver.getCurrentUrl();
		if (actualUrl.contains(profileUrl)) {
			return true;
		}else
			return false;
	}
	
	public boolean navigateToReview() {
		navigateToReviewButton.click();
		return yourReviewText.isDisplayed();
	}
}
