package com.wallethub.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ConfirmationScreenPage {
	
	WebDriver driver;
	
	public ConfirmationScreenPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy (xpath = "//div[@class='rvc-header']/h4")
	WebElement successMsg;
	
	@FindBy (xpath = "//div[contains(@class,'btn rvc-continue-btn')]")
	WebElement continueButton;
	
	@FindBy (xpath = "//div[@id='more-list-menu']/following-sibling::div/span")
	WebElement profileName;
	
	@FindBy (xpath = "//div[@id='more-list-menu']/following-sibling::div/div/a[contains(text(),'Profile')]")
	WebElement profile;
	
	public boolean isSubmittedSuccessfully(String successMessage) {
		
		if (successMsg.getText().trim().equalsIgnoreCase(successMessage)) {
			return true;
		}else
			return false;
	}
	
	
	public void selectProfile() {
		profileName.click();
		profile.click();
	}
	
	
	
}
