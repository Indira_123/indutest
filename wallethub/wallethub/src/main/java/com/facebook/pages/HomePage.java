package com.facebook.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {
	
	WebDriver driver;
	
	public HomePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	//create post webelement
	@FindBy (xpath="//span[contains(text(),\"What's on your mind\")]//parent::div/parent::div")
	public WebElement createPostTextBox;
	
	@FindBy (xpath="//div[contains(@aria-label,\"What's on your mind\")]")
	public WebElement createPostPopupTestBox;

	// Post button webelement
	@FindBy (xpath="//span[contains(text(),\"Post\")]")
	public WebElement postButton;
	
	// Post button webelement
		@FindBy (xpath="//div[contains(text(),\"Hello World\")]")
		public WebElement postedStatus;
	
		
	public void postMessage(String msg) throws InterruptedException {
		createPostTextBox.click();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement clickPostPopup = wait.until(ExpectedConditions.elementToBeClickable(createPostPopupTestBox));
		clickPostPopup.sendKeys(msg);
		WebElement clickPost = wait.until(ExpectedConditions.elementToBeClickable(postButton));
		clickPost.click();
	}
	
	public boolean verifyPostedStatus() {
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].scrollIntoView(true);", postedStatus);
		return postedStatus.isDisplayed();
	}
	

}
