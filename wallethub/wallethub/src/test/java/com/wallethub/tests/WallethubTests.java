package com.wallethub.tests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.utils.BaseClass;
import com.wallethub.pages.ConfirmationScreenPage;
import com.wallethub.pages.LoginPage;
import com.wallethub.pages.ProfilePage;
import com.wallethub.pages.ReviewSubmitPage;
import com.wallethub.pages.TestInsuranceCompanyReviewPage;

public class WallethubTests {
	
	WebDriver driver;
	Properties props;
	TestInsuranceCompanyReviewPage insuranceCompanyReviewPage;
	LoginPage loginPage;
	ReviewSubmitPage reviewSubmitPage;
	ConfirmationScreenPage confirmScreenPage;
	ProfilePage profilePage;
	
	@BeforeSuite
	public void loadProperties() throws IOException {
		File file = new File(".//WallethubObjects.properties");
		FileInputStream fis = new FileInputStream(file);
		props = new Properties();
		props.load(fis);
	}
	
	@BeforeTest
	public void launchFacebook() {
		BaseClass base = new BaseClass();
		driver = base.launchBrowser(props.getProperty("url"), props.getProperty("browserName"));
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}
	
	@Test
	public void postReviewOnWalletHub() {
		
		loginPage = new LoginPage(driver);
		loginPage.loginToWalletHub(props.getProperty("email"), props.getProperty("password"));
		
		insuranceCompanyReviewPage = new TestInsuranceCompanyReviewPage(driver);
		insuranceCompanyReviewPage.goToTestInsuranceCompanyReviewURL(driver, props.getProperty("testInsuranceCompanyURL"));
		boolean isLitUp = insuranceCompanyReviewPage.hoverAndVerifyStarsLitUp(props.getProperty("expectedColorCode"));
		Assert.assertTrue(isLitUp, "Stars not highlighted");
		insuranceCompanyReviewPage.selectRating(Integer.parseInt(props.getProperty("rating")));
		
		reviewSubmitPage = new ReviewSubmitPage(driver);
		reviewSubmitPage.selectInsuranceType("Health Insurance");
		reviewSubmitPage.submitYourReview(props.getProperty("reviewMsg"));
		confirmScreenPage = new ConfirmationScreenPage(driver);
		boolean isSubmitted = confirmScreenPage.isSubmittedSuccessfully(props.getProperty("successMessage"));
		Assert.assertTrue(isSubmitted, "Review NOT submitted successfully");
		confirmScreenPage.selectProfile();
		
		profilePage = new ProfilePage(driver);
		boolean profileUrl = profilePage.verifyIfNavigatedToProfilePage(props.getProperty("profileUrl"));
		Assert.assertTrue(profileUrl, "Page is NOT navigated to Profile");
		boolean givenRating = profilePage.verifyGivenRating(Integer.parseInt(props.getProperty("rating")));
		Assert.assertTrue(givenRating, "Rating is NOT as given by user");
		boolean navigateToReview = profilePage.navigateToReview();
		Assert.assertTrue(navigateToReview, "User is not navigated to Review Page");
		
		insuranceCompanyReviewPage = new TestInsuranceCompanyReviewPage(driver);
		String actualReviewText = insuranceCompanyReviewPage.getReviewText();
		Assert.assertEquals(actualReviewText.trim(), props.getProperty("reviewMsg"));

		
	}
	
	
	
	
	
	@AfterTest
	public void tearDown() {
//		driver.quit();
	}

	
	

}
