package com.facebook.tests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.facebook.pages.HomePage;
import com.facebook.pages.LoginPage;
import com.utils.BaseClass;

public class PostStatusMessageOnFacebook {
	WebDriver driver;
	LoginPage loginPage;
	HomePage homePage;
	Properties props;

	@BeforeSuite
	public void loadProperties() throws IOException {
		File file = new File(".//FacebookObjects.properties");
		FileInputStream fis = new FileInputStream(file);
		props = new Properties();
		props.load(fis);
	}

	@BeforeTest
	public void launchFacebook() {
		BaseClass base = new BaseClass();
		driver = base.launchBrowser(props.getProperty("url"), props.getProperty("browserName"));
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}

	@Test
	public void loginToFacebookAndPostStatusMessage() throws InterruptedException {

		loginPage = new LoginPage(driver);
		loginPage.loginFacebook(props.getProperty("username"), props.getProperty("password"));

		homePage = new HomePage(driver);
		Assert.assertTrue(homePage.createPostTextBox.isDisplayed(), "Create Post text box is not found/enabled");
		System.out.println("Home page is successfully loaded and Create Post text box is enabled");
		
		homePage.postMessage(props.getProperty("postMessage"));
		Assert.assertTrue(homePage.verifyPostedStatus(), "Status is not found");
		System.out.println("Status is posted successfully");

	}

	@AfterTest
	public void tearDown() {
//		driver.quit();
	}

}
