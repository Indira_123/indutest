package com.practice;

public class SwapTwoNumbers {

	public static void main(String[] args) {

		
		int a=34, b=201;      
		System.out.println(a+b);      
		a=a+b;//a=30 (10+20)    
		b=a-b;//b=10 (30-20)    
		a=a-b;//a=20 (30-10)    
		System.out.println(a+ " " +b);   
		
		//Another way
		a=34; b=201; 
		a=a*b;//a=30 (10+20)    
		b=a/b;//b=10 (30-20)    
		a=a/b;//a=20 (30-10)    
		System.out.println(a+ " " +b);   
		
		//Another way with temp variable
		a=34;b=201;int t;
		t=a;
		a=b;
		b=t;
		System.out.println(a+ " " +b);   
		
		//Another way with single statement
		a=34;b=201;
		b=a+b-(a=b);
		System.out.println(a+ " " +b);   
		
	}

}
