package com.practice;

public class SumOfDigitsInANumber {

	public static void main(String[] args) {
			int a = 123789;
			int r = 0;
			while(a!=0) {
				r=r+a%10;
				a=a/10;
			}
			System.out.println("Sum of given number is "+r);
	}

}
