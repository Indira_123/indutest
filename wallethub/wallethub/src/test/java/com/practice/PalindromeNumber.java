package com.practice;

public class PalindromeNumber {

	public static void main(String[] args) {
		int a = 168461;
		int n=0; int b=a;
		while(a!=0) {
			n=n*10+a%10;
			a=a/10;
		}
		if(n==b) {
			System.out.println("Palindrome number");
		}
		else {
			System.out.println("Not Palindrome number");
		}
	}

}
