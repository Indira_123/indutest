package com.practice.selenium;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.testng.IAnnotationTransformer;
import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;
import org.testng.annotations.ITestAnnotation;

public class IAnnotationTransformerTest implements IAnnotationTransformer {

		  public void transform(ITestAnnotation annotation, Class testClass, Constructor testConstructor, Method testMethod) {
//		    annotation.setInvocationCount(0);
//		    annotation.setThreadPoolSize(25);
		    annotation.setRetryAnalyzer(RetryAnalyzerClass.class);
		  }
		}

