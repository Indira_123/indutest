package com.practice.selenium;

import java.io.File;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ITestListenerTest implements ITestListener{

	
	ExtentReports reports;
	ThreadLocal<ExtentTest> local;
	public void onStart(ITestContext context) {
		File file = new File("HTMLReport.html"); 
		ExtentHtmlReporter extentHtml = new ExtentHtmlReporter(file);
		reports = new ExtentReports();
		reports.attachReporter(extentHtml);;
	}
	/* (non-Javadoc)
	 * @see org.testng.ITestListener#onTestStart(org.testng.ITestResult)
	 */
	public void onTestStart(ITestResult result) {
		local = new ThreadLocal<ExtentTest>();
		ExtentTest extentTest = reports.createTest(result.getMethod().getMethodName());
		local.set(extentTest);
	}

	/* (non-Javadoc)
	 * @see org.testng.ITestListener#onTestSuccess(org.testng.ITestResult)
	 */
	public void onTestSuccess(ITestResult result) {
		String log = "Test Failed : "+result.getMethod().getMethodName();
		Markup m = MarkupHelper.createLabel(log, ExtentColor.GREEN);
		local.get().log(Status.PASS, "Test Successful");
	}

	/* (non-Javadoc)
	 * @see org.testng.ITestListener#onTestFailure(org.testng.ITestResult)
	 */
	public void onTestFailure(ITestResult result) {
//		RetryAnalyzerClass c = new RetryAnalyzerClass();
//		c.retry(result);
		String log = "Test Failed : "+result.getMethod().getMethodName();
		Markup m = MarkupHelper.createLabel(log, ExtentColor.RED);
		local.get().log(Status.FAIL, "Test Failed");
	}
	

	/* (non-Javadoc)
	 * @see org.testng.ITestListener#onTestSkipped(org.testng.ITestResult)
	 */
	public void onTestSkipped(ITestResult result) {
	}

	/* (non-Javadoc)
	 * @see org.testng.ITestListener#onTestFailedButWithinSuccessPercentage(org.testng.ITestResult)
	 */
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
	}

	/* (non-Javadoc)
	 * @see org.testng.ITestListener#onTestFailedWithTimeout(org.testng.ITestResult)
	 */
	public void onTestFailedWithTimeout(ITestResult result) {
	}

	/* (non-Javadoc)
	 * @see org.testng.ITestListener#onStart(org.testng.ITestContext)
	 */


	/* (non-Javadoc)
	 * @see org.testng.ITestListener#onFinish(org.testng.ITestContext)
	 */
	public void onFinish(ITestContext context) {
		reports.flush();
	}
	
}
