package com.practice.selenium;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Test (singleThreaded=true)
@Listeners ({com.practice.selenium.ITestListenerTest.class, com.practice.selenium.IAnnotationTransformerTest.class})
public class CallingPropertiesFile {
	
	Properties props ;
	@BeforeTest 
	public void beforeTest() throws IOException {
		File file = new File("./WallethubObjects.properties");
		FileInputStream fis = new FileInputStream(file);
		props = new Properties();
		props.load(fis);
		System.out.println("before test");
	}
	
	@Test (invocationCount=5, skipFailedInvocations=true, dependsOnMethods= {"test2"}, dependsOnGroups= {"Group1", "Group2"},
			dataProvider="data", dataProviderClass=com.practice.selenium.CallingPropertiesFile.class)
	public void test() {
		System.out.println(props.getProperty("browserName"));
		System.out.println("test");
	}
	@Test (alwaysRun=true, expectedExceptions=IOException.class, retryAnalyzer=RetryAnalyzerClass.class,
			groups= {"Group1", "Group2"}, timeOut=10)
	public void test2() {
		System.out.println(props.getProperty("browserName"));
		System.out.println("test");
	}
	
	@AfterTest
	public void afterTest() {
		System.out.println("test");
	}

}
