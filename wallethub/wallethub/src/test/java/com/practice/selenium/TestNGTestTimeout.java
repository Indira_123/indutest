package com.practice.selenium;

import org.testng.annotations.Test;

public class TestNGTestTimeout {
	
	
	@Test(timeOut=120, expectedExceptions=ArithmeticException.class)
	public void method()  {
			int a = 1/0;
		System.out.println("Timeout test");
	}

}
