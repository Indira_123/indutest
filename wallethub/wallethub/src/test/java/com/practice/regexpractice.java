package com.practice;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class regexpractice {
	
	public static void main(String[] args) {
		
		Pattern p =  Pattern.compile("sa");	
		
		Matcher m = p.matcher("sa");
		boolean b = m.matches();
		System.out.println(b);
		
		System.out.println(Pattern.matches("..s", "Ansar"));
		System.out.println(Pattern.matches("..s..", "Ansar"));
		System.out.println(Pattern.matches("..s", "AnS"));
		System.out.println(Pattern.matches("[sa]", "Ansar"));
		System.out.println(Pattern.matches("[amn]", "abcd"));//false (not a or m or n)  
		System.out.println(Pattern.matches("[amn]", "a"));//true (among a or m or n)  
		System.out.println(Pattern.matches("[amn]", "ammmna"));//false (m and a comes more than once)  
	}

}
