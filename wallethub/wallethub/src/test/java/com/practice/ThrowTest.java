package com.practice;

import java.awt.List;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;

public class ThrowTest {
	
	public int var =5; 

	public static void main(String[] args) throws IllegalAccessException, InstantiationException {
		
		System.out.println("Hi");
		try {
				
					throw new ArithmeticException();
				
			
		}catch (ArithmeticException a) {
			a.getMessage();
		}
		
		// InstantiationException
		
		// Thrown when an application tries to create an instance of a class using the newInstance method in class Class , but the specified class object cannot be instantiated because it is an interface or is an abstract class.
//		Class<Collection> t = Collection.class;
//		Collection tt = t.newInstance();
//		System.out.println(tt.getClass());
		
		
	}
}
