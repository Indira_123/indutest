package com.practice;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class FindDuplicateWordsInAString {

	public static void main(String[] args) {
			String s = "hi hi hi me you test";
			String[] a = s.split(" ");
			Map<String,Integer> m = new HashMap<String, Integer>();
			for(String b:a) {
				if(m.containsKey(b)) {
					m.put(b, m.get(b)+1);
				}
				else {
					m.put(b, 1);
				}
			}
			Set<String> keys = m.keySet();
			for(String key:keys) {
				System.out.println(key + " is repeated "+ m.get(key) + " times");
			}
			 Set<Entry<String, Integer>> count = m.entrySet();
			 for(Entry<String, Integer> j : count) {
				 System.out.println("The word " + j.getKey() + " is repeated " + j.getValue() + " times");
			 }
	}

}
