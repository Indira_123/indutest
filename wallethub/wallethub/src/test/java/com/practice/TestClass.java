package com.practice;

public class TestClass {
	
	public static void main(String[] args) {
		
		
		ClassOne one = new ClassOne();
		// Can access methods of only ClassOne
		// Overridden methods not called
		one.methodOne();
		one.methodTwo();
		one.methodThree();
		
		BasicMethods b = new BasicMethods();
		b.test1();
		
		ClassOne two = new ClassTwo();
		// Can access methods of only ClassOne
		// Overridden method from ClassTwo is called, though method also overridden in ClassThree
		// Method body from ClassOne is ignored and its overridden implementation from ClassTwo is called
		two.methodOne();
		two.methodTwo();
		two.methodThree();
		
		ClassOne three = new ClassThree();
		// Can access methods of only ClassOne
		// Overridden method from ClassThree is called, though method also overridden in ClassTwo
		// Method body from ClassOne and ClassTwo is ignored and its overridden implementation from ClassThree is called
		three.methodOne();
		three.methodTwo();
		three.methodThree();
		
		ClassTwo five = new ClassTwo();
		// Able to access methods from ClassOne & ClassTwo
		// Called Overridden method body in ClassTwo, not from ClassOne  
		five.methodOne();
		five.methodTwo();
		five.methodThree();
		five.methodFour();
		five.methodFive();
		five.methodSix();
		
		// Cant use ClassThree six = new ClassTwo();
		ClassOne six = new ClassTwo();
		// Able to access methods from only ClassOne
		// Overridden method body from ClassTwo is called, not from ClassOne
		six.methodOne();
		six.methodTwo();
		six.methodThree();
		
		
		ClassThree seven = new ClassThree();
		// Able to access all methods from ClassOne, ClassTwo, ClassThree
		// ClassOne methods which are overridden inside ClassTwo are called
		// ClassOne or ClassTwo methods which are overridden inside ClassThree are called
		seven.methodOne();
		seven.methodTwo();
		seven.methodThree();
		seven.methodFour();
		seven.methodFive();
		seven.methodSix();
		seven.methodSeven();
		seven.methodEight();
		seven.methodNine();
		
		ClassTwo eight = new ClassThree();
		// Able to access all methods of ClassTwo and ClassOne
		// ClassOne methods which are overridden inside ClassTwo are called
		// ClassOne or ClassTwo methods which are overridden inside ClassThree are called
		eight.methodOne();
		eight.methodTwo();
		eight.methodThree();
		eight.methodFour();
		eight.methodFive();
		eight.methodSix();
		
		ClassOne Nine = new ClassThree();
		// Able to access all methods of ClassOne
		// ClassOne methods which are overridden inside ClassTwo are called
		// ClassOne methods which are overridden inside ClassTwo and again overridden in ClassThree are called
		Nine.methodOne();
		Nine.methodTwo();
		Nine.methodThree();
		
	}

}
