package com.practice;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class SearchForAStringInAFile {

	public static void main(String[] args) throws IOException {
			File file = new File("./output.txt");
			FileReader r = new FileReader(file);
			Scanner s = new Scanner(r);
			
			
			while(s.hasNextLine()) {
				if((s.nextLine()).contains("test")) {
					System.out.println("Word is present");
				}
			}
			s.close();
	}

}
