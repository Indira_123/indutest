package com.practice.two;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class FindDuplicateElementsOfArrayListUsingSet {

	public static void main(String[] args) {
		
		ArrayList<String> list = new ArrayList<String>();
		
		for (int i=0,j=0; i<10; i++) {
			list.add(String.valueOf(i));
			if (i<j+6) {
				list.add(String.valueOf(i));
			}
		}
		list.add(String.valueOf(-154));
		list.add(String.valueOf(-154));
		System.out.println(list);
		
		Set<String> resultSet = new HashSet<String>();
		Set<String> tempSet = new HashSet<String>();
		for (String s : list) {
			if (!tempSet.add(s)) {
				resultSet.add(s);
			}
		}
		
		System.out.println(resultSet);
		
	}
}
