package com.practice.two;

public class ReverseANumber {

	public static void main(String[] args) {
		
		int number = 324;
		int i=0;
		while (number>0) {
			i= i*10 + number%10;
			number = number/10;
		}
		
		System.out.println(i);
	}

}
