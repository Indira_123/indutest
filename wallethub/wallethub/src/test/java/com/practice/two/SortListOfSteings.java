package com.practice.two;

import java.util.Arrays;
import java.util.Collections;

public class SortListOfSteings {

	public static void main(String[] args) {

		String[] listOfStrings = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul","aug", "Sep", "Oct", "nov", "Dec" };
		
		// Alphabetical Order
		Arrays.sort(listOfStrings);
		for (String s: listOfStrings) {
			System.out.print(s+ ", ");
		}
		
		Arrays.sort(listOfStrings,String.CASE_INSENSITIVE_ORDER);
		for (String s: listOfStrings) {
			System.out.print(s+ ", ");
		}
	}

}
