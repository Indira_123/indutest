package com.practice.two;

import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.temporal.ChronoField;

public class DateAndTime {


	public static void main(String[] args) {
		
		LocalDate ld= LocalDate.now();
		System.out.println(ld); // Date as 2021-07-15
		System.out.println(ld.getDayOfWeek()); // SATURDAY
		System.out.println(ld.getDayOfMonth()); // 15
		System.out.println(ld.getDayOfYear()); //190
		
		Clock c = Clock.system(ZoneId.of("GMT-5"));
		
		LocalDateTime ldt = LocalDateTime.now(c);
		System.out.println(ldt.plus(Period.ofDays(-1)));
		
		LocalDateTime ldt2 = LocalDateTime.of(2020,5,13,20,45);
		System.out.println(ldt2.get(ChronoField.HOUR_OF_DAY)+ldt2.getDayOfMonth());
		System.out.println(ChronoField.HOUR_OF_DAY);
		
		
		
		
	}

}
