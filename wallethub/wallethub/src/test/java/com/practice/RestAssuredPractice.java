package com.practice;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

public class RestAssuredPractice {

	@Test
	public void verifyResponseCode() {

//		Response response = RestAssured.get("https://reqres.in/api/users?page=2");
//		System.out.println(response.getStatusCode());
//		System.out.println(response.toString());
//		System.out.println(response.getBody());

		given().get("https://reqres.in/api/users?page=2").then().statusCode(200);
	}

	@Test
	public void getResponse() {

		given().header("Content-Type", "application/json").get("https://reqres.in/api/users?page=2").then()
				.body("data.id[0]", equalTo(7)).body("data.first_name", hasItems("Michael", "Lindsay"));

		given().get("https://reqres.in/api/users?page=2").then().statusCode(200).log().all();
	}

	@Test
	public void test01_POST() {
		// For creating a resource on server
		
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("name", "Ansar");
		map.put("job", "Test Analyst");
		

		JSONObject req = new JSONObject(map);
		System.out.println(req);
		req.put("Age", "28");
		System.out.println(req.toJSONString());
		
	}

}
