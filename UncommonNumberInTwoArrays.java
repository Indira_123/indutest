package com.practice;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class UncommonNumberInTwoArrays {
	
	public static void main(String[] args) {
		int[] a = {1,2,3,4,5};
		int[] b = {1,2,3,6,7};
		
		int[] d = new int[a.length+b.length];
		
		for(int i=0; i<a.length; i++) {
			d[i] = a[i];
		}
		for(int i=a.length; i<a.length+b.length; i++) {
			d[i] = b[i-a.length];
		}
		int counts=0;
		for(int k=0; k<d.length; k++) {
			for(int j=0; j<d.length; j++) {
				if (d[k]==d[j]) {
					counts++;
				}
			}
			
			if (counts<2) {
				System.out.println(d[k]);
			}
			counts=0;
		}
		
		// Approach two
//		List<int[]> list1 = Arrays.asList(a);
//		List<int[]> list2 = Arrays.asList(b);
//		System.out.println(list1.get(0)[0]);
		
		
		}
}
